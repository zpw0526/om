package com.zero2oneit.mall.member.controller;

import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignQueryObject;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import com.zero2oneit.mall.common.query.member.MemberSignRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.service.MemberSignRecordService;
import com.zero2oneit.mall.member.service.MemberSignRuleService;
import com.zero2oneit.mall.member.service.MemberSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
@RestController
@RequestMapping("/admin/member/sign")
public class MemberSignController {

    @Autowired
    private MemberSignRuleService memberSignRuleService;

    @Autowired
    private MemberSignRecordService memberSignRecordService;

    @Autowired
    private MemberSignService memberSignService;

    /**
     * 查询签到规则列表信息
     * @param qo
     * @return
     */
    @PostMapping("/ruleList")
    public BoostrapDataGrid ruleList(@RequestBody MemberSignRuleQueryObject qo){
        return memberSignRuleService.ruleList(qo);
    }

    /**
     * 添加或编辑签到规则信息
     * @param memberSignRule
     * @return
     */
    @PostMapping("/ruleAddOrEdit")
    public R ruleAddOrEdit(@RequestBody MemberSignRule memberSignRule){
        memberSignRuleService.saveOrUpdate(memberSignRule);
        return R.ok();
    }

    /**
     * 查询签到记录列表信息
     * @param qo
     * @return
     */
    @PostMapping("/recordList")
    public BoostrapDataGrid recordList(@RequestBody MemberSignRecordQueryObject qo){
        return memberSignRecordService.recordLists(qo);
    }

    /**
     * 查询签到列表信息
     * @param qo
     * @return
     */
    @PostMapping("/signList")
    public BoostrapDataGrid signList(@RequestBody MemberSignQueryObject qo){
        return memberSignService.signList(qo);
    }

}
