package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.MemberSign;
import com.zero2oneit.mall.common.query.member.MemberSignQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Tg
 * @create 2021-05-13
 * @description
 */
@Mapper
public interface MemberSignMapper extends BaseMapper<MemberSign> {

    int selectTotal(MemberSignQueryObject qo);

    List<Map<String, Object>> selectAll(MemberSignQueryObject qo);

}
