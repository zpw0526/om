let obj = {
	required: true,
	message: '必填项，请输入',
	trigger: 'blur',
};
let check = {
	required: true,
	message: '必选项，请选择',
	trigger: 'change',
};
let num = {
	type: 'number',
	message: '只能为数字',
	trigger: ['change','blur'],
};
let length = {
	min: 1,
	max: 10,
	message: '简称长度在1到10个字符',
	trigger: ['change','blur'],
};

//申请表单一规则
let applyRule1 = {
	busiLicense:[                              //营业执照，必选
		{
			required: true,
			message: '请选择图片上传',
			trigger: 'change' ,
		},
	],
	busiArea: [check],                         //合作地区，必选
	busiName: [obj,length],                    //商家简称，必填，长度在1-10之间
	busiType: [check],                         //公司类型，必选
	busiTurnover: [obj,num],                   //近一年营业额，必填，只能为数字
	payDuty: [check],                          //纳税级别，必选
	busiService: [obj,length],                 //业务联系人，必填，长度在1-10之间
	addressArea: [check],                      //办公地区，必选
	address: [obj],                            //详细地址，必填
	storeArea: [check],                        //仓库地区，必选
	storeSize: [obj,num]                       //仓库面积，必填，只能为数字
};

//申请表单二规则
let applyRule2 = {
	busiAttribute: [check],                    //商家属性，必选
	typeName: [check],                         //主营品类名称，必选
	sourcesSupply: [check],                    //商品供货来源，必选
	supplyArea: [check],                       //供货省市，必选
	carCount: [	obj,num],                      //自有车辆数量，必填，只能为数字
	cooperatePlatform: [check]                 //合作平台，必选
};

//公司类型选项列表
let listBusiType = [
	{
		value: '个体工商户',
		label: '个体工商户'
	},
	{
		value: '国企',
		label: '国企'
	},
	{
		value: '合资企业',
		label: '合资企业'
	},
	{
		value: '民营企业',
		label: '民营企业'
	},
	{
		value: '责任公司',
		label: '责任公司'
	},
	{
		value: '股份公司',
		label: '股份公司'
	},
	{
		value: '独资企业',
		label: '独资企业'
	},
	{
		value: '上市公司',
		label: '上市公司'
	},
	{
		value: '外资企业',
		label: '外资企业'
	},
	{
		value: '其他',
		label: '其他'
	}
];

//纳税级别选项列表
let listPayDuty = [
	{
		value: '一般纳税人',
		label: '一般纳税人'
	},
	{
		value: '小规模纳税人',
		label: '小规模纳税人'
	}
];

//商家属性选择列表
let listBusiAttribute = [
	{
		value: '生厂商',
		label: '生厂商'
	},
	{
		value: '品牌厂商',
		label: '品牌厂商'
	},
	{
		value: '基地',
		label: '基地'
	},
	{
		value: '全国代理',
		label: '全国代理'
	},
	{
		value: '大区代理',
		label: '大区代理'
	},
	{
		value: '城市代理',
		label: '城市代理'
	},
	{
		value: '区县代理',
		label: '区县代理'
	},
	{
		value: '贸易商',
		label: '贸易商'
	},
	{
		value: '进出口商',
		label: '进出口商'
	}
];

//商家供货来源选择列表
let listSourcesSupply = [
	{
		value: '自有工厂',
		label: '自有工厂'
	},
	{
		value: '批发商/经销商/贸易商',
		label: '批发商/经销商/贸易商'
	},
	{
		value: '一件代发',
		label: '一件代发'
	}
];

//合作平台选择列表
let listCooperatePlatform = [
	{
		value: '无',
		label: '无'
	},
	{
		value: '美团优选',
		label: '美团优选'
	},
	{
		value: '拼多多',
		label: '拼多多'
	},
	{
		value: '淘宝/天猫',
		label: '淘宝/天猫'
	},
	{
		value: '京东',
		label: '京东'
	},
	{
		value: '十荟团',
		label: '十荟团'
	},
	{
		value: '多多买菜',
		label: '多多买菜'
	},
	{
		value: '橙心优选',
		label: '橙心优选'
	},
	{
		value: '兴盛优选',
		label: '兴盛优选'
	},
	{
		value: '超市/便利店',
		label: '超市/便利店'
	}
];

module.exports = {
	applyRule1,
	applyRule2,
	listBusiType,
	listPayDuty,
	listBusiAttribute,
	listSourcesSupply,
	listCooperatePlatform,
};
